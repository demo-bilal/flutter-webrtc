import 'dart:convert';

import 'package:anomic/utils/user_utils.dart';
import 'package:http/http.dart' as http;

String meetingApiUrl = "http://192.168.0.16:4000/api/meeting";
var client = http.Client();

Future<http.Response?> startMeeting() async {
  Map<String, String> header = {
    "Content-Type": "application/json",
  };

  var userId = await loadUserId();

  var res = await http.post(
    Uri.parse("$meetingApiUrl/start"),
    headers: header,
    body: jsonEncode({"hostId": userId, "hostName": ""}),
  );

  if (res.statusCode == 200) {
    print(res.body);
    return res;
  } else {
    return null;
  }
}

Future<http.Response?> joinMeeting(String meetingId) async {
  Map<String, String> header = {
    "Content-Type": "application/json",
  };

  var res = await http.get(
    Uri.parse("$meetingApiUrl/join?meetingId=$meetingId"),
    headers: header,
  );

  if (res.statusCode == 200) {
    return res;
  }

  throw UnsupportedError("not a valid Meeting");
}
