import 'package:anomic/components/control_panel.dart';
import 'package:anomic/components/remote_connection.dart';
import 'package:anomic/models/meeting_detail_model.dart';
import 'package:anomic/pages/home_screen.dart';
import 'package:anomic/utils/user_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_wrapper/flutter_webrtc_wrapper.dart';

class MeetingPage extends StatefulWidget {
  final String meetingId;
  final String? name;
  final MeetingDetailModel meetingDetailModel;

  MeetingPage({
    super.key,
    required this.meetingId,
    this.name,
    required this.meetingDetailModel,
  });

  @override
  State<MeetingPage> createState() => _MeetingPageState();
}

class _MeetingPageState extends State<MeetingPage> {
  final RTCVideoRenderer _localRenderer = RTCVideoRenderer();
  final Map<String, dynamic> mediaConstraints = {
    "audio": true,
    "video": true,
  };
  bool isConnectionFailed = false;
  WebRTCMeetingHelper? meetingHelper;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      body: _buildMeetingRoom(),
      bottomNavigationBar: ControlPanel(
        onVideoToggle: onVideoToggle,
        onAudioToggle: onAudioTaggle,
        videoEnabled: isVideoEnebled(),
        audioEnabled: isAudioEnebled(),
        isConnectionFailed: isConnectionFailed,
        onReconnect: handleReconnect,
        onMeetingEnd: onMeetingEnd,
      ),
    );
  }

  Widget _buildMeetingRoom() {
    return Stack(
      children: [
        meetingHelper != null && meetingHelper!.connections.isNotEmpty
            ? GridView.count(
                crossAxisCount: meetingHelper!.connections.length < 3 ? 1 : 2,
                children:
                    List.generate(meetingHelper!.connections.length, (index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RemoteConnection(
                      renderer: meetingHelper!.connections[index].renderer,
                      connection: meetingHelper!.connections[index],
                    ),
                  );
                }))
            : const Center(
                child: Text(
                  "Toplantıya katılacak kullanıcılar bekleniyor  --- meeting_page.dart : 57",
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.yellow,
                  ),
                ),
              ),
        Positioned(
          bottom: 10,
          right: 0,
          child: SizedBox(
            width: 150,
            height: 200,
            child: RTCVideoView(
              _localRenderer,
            ),
          ),
        ),
      ],
    );
  }

  bool? isVideoEnebled() {
    return meetingHelper != null ? meetingHelper!.videoEnabled! : false;
  }

  bool? isAudioEnebled() {
    return meetingHelper != null ? meetingHelper!.audioEnabled! : false;
  }

  onVideoToggle() {
    if (meetingHelper != null) {
      setState(() {
        meetingHelper!.toggleVideo();
      });
    }
  }

  onAudioTaggle() {
    if (meetingHelper != null) {
      setState(() {
        meetingHelper!.toggleAudio();
      });
    }
  }

  handleReconnect() {
    if (meetingHelper != null) {
      meetingHelper!.reconnect();
    }
  }

  void startMeeting() async {
    print("---------  start Meeting");
    final String userId = await loadUserId();
    meetingHelper = WebRTCMeetingHelper(
      url: "http://192.168.0.16:4000", //api/meeting/join",
      meetingId: widget.meetingDetailModel.id,
      userId: userId,
      name: widget.name,
    );

    MediaStream _loaclStream =
        await navigator.mediaDevices.getUserMedia(mediaConstraints);

    _localRenderer.srcObject = _loaclStream;
    meetingHelper!.stream = _loaclStream;

    meetingHelper!.on("open", context, (ev, context) {
      print("meetingPage.dart ----- open");
      setState(() {
        isConnectionFailed = false;
      });
    });

    meetingHelper!.on("connection", context, (ev, context) {
      print("meetingPage.dart ----- connection");

      setState(() {
        isConnectionFailed = false;
      });
    });

    meetingHelper!.on("user-left", context, (ev, context) {
      print("meetingPage.dart ----- user-left");
      setState(() {
        isConnectionFailed = false;
      });
    });

    meetingHelper!.on("video-toggle", context, (ev, context) {
      setState(() {});
    });

    meetingHelper!.on("audio-toggle", context, (ev, context) {
      setState(() {});
    });

    meetingHelper!.on("meeting-ended", context, (ev, context) {
      print("meetingPage.dart ----- meeting-ended");
      onMeetingEnd();
    });

    meetingHelper!.on("connection-setting-changed", context, (ev, context) {
      setState(() {
        isConnectionFailed = false;
      });
    });

    meetingHelper!.on("stream-changed", context, (ev, context) {
      setState(() {
        isConnectionFailed = false;
      });
    });

    setState(() {});
  }

  initRenderers() async {
    await _localRenderer.initialize();
  }

  @override
  void initState() {
    super.initState();
    initRenderers();
    startMeeting();
  }

  @override
  void deactivate() {
    super.deactivate();
    _localRenderer.dispose();
    if (meetingHelper != null) {
      meetingHelper!.destroy();
      meetingHelper = null;
    }
  }

  void goToHomePage() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => const HomeScreen()),
    );
  }

  void onMeetingEnd() {
    if (meetingHelper != null) {
      meetingHelper!.endMeeting();
      meetingHelper = null;
      goToHomePage();
    }
  }
}
