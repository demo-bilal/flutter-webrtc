import 'dart:convert';

import 'package:anomic/api/meeting_api.dart';
import 'package:anomic/models/meeting_detail_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'join_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController meetingIdCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ana sayfa"),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            TextFormField(
              controller: meetingIdCtrl,
              decoration: const InputDecoration(hintText: "meeting Id YAZ"),
            ),
            const SizedBox(height: 10),
            TextButton(
              onPressed: () {
                if (meetingIdCtrl.text.length > 0) {
                  validateMeeting(meetingIdCtrl.text.trim());
                } else {
                  print("home_screen ::: 40 - MEETİNG ID ALANINI DOLDUR");
                }
              },
              child: const Text("join meeting"),
            ),
            const SizedBox(height: 10),
            TextButton(
              onPressed: () async {
                var response = await startMeeting();
                final body = json.decode(response!.body);
                final meetingId = body['data'];

                validateMeeting(meetingId);
              },
              child: const Text("start meeting- ODA OLUSTUR"),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  void validateMeeting(String meetingId) async {
    try {
      http.Response? response = await joinMeeting(meetingId);
      if (response == null) {
        throw "response NULL";
      }

      var data = json.decode(response.body);

      final meetingDetails = MeetingDetailModel.fromJson(data["data"]);

      goToJoinScreen(meetingDetails);
    } catch (e) {
      print("home_screen.dart :::: 61 CATCH ERROR : $e");
    }
  }

  goToJoinScreen(MeetingDetailModel meetingDetailModel) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => JoinScreen(
          meetingDetailModel: meetingDetailModel,
        ),
      ),
    );
  }
}
