import 'package:anomic/models/meeting_detail_model.dart';
import 'package:anomic/pages/meeting_page.dart';
import 'package:flutter/material.dart';

class JoinScreen extends StatefulWidget {
  final MeetingDetailModel? meetingDetailModel;

  // ignore: prefer_const_constructors_in_immutables
  JoinScreen({
    super.key,
    this.meetingDetailModel,
  });

  @override
  State<JoinScreen> createState() => _JoinScreenState();
}

class _JoinScreenState extends State<JoinScreen> {
  TextEditingController usernameCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("join screen")),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TextFormField(
              controller: usernameCtrl,
              decoration: const InputDecoration(hintText: "userName YAZ"),
            ),
            const SizedBox(height: 10),
            TextButton(
              onPressed: () {
                if (usernameCtrl.text.length > 1) {
                  //meeting
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => MeetingPage(
                        name: usernameCtrl.text,
                        meetingId: widget.meetingDetailModel!.id!,
                        meetingDetailModel: widget.meetingDetailModel!,
                      ),
                    ),
                  );
                } else {
                  print("join_screen : 36 - ALANLARI DOLDUR");
                }
              },
              child: const Text("join"),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
