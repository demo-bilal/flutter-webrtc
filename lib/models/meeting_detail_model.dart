class MeetingDetailModel {
  String? id;
  String? hostId;
  String? hostName;

  MeetingDetailModel({
    this.id,
    this.hostId,
    this.hostName,
  });

  factory MeetingDetailModel.fromJson(dynamic json) {
    return MeetingDetailModel(
      id: json["id"],
      hostId: json["hostId"],
      hostName: json["hostName"],
    );
  }
}
