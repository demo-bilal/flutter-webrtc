import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

var uuid = const Uuid();

Future<String> loadUserId() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  String? userId;
  if (pref.containsKey("userId")) {
    userId = pref.getString("userId");
  } else {
    userId = uuid.v4();
    pref.setString("userId", userId);
  }
  return userId.toString();
}
