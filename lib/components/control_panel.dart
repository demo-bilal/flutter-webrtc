import 'package:flutter/material.dart';

class ControlPanel extends StatelessWidget {
  final bool? videoEnabled;
  final bool? audioEnabled;
  final bool? isConnectionFailed;
  final VoidCallback? onVideoToggle;
  final VoidCallback? onAudioToggle;
  final VoidCallback? onReconnect;
  final VoidCallback? onMeetingEnd;

  const ControlPanel({
    super.key,
    this.videoEnabled,
    this.audioEnabled,
    this.isConnectionFailed,
    this.onVideoToggle,
    this.onAudioToggle,
    this.onReconnect,
    this.onMeetingEnd,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      color: Colors.grey,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: buildControls(),
      ),
    );
  }

  List<Widget> buildControls() {
    if (isConnectionFailed == false) {
      return [
        IconButton(
          onPressed: onVideoToggle,
          icon: Icon(
            videoEnabled == true ? Icons.videocam : Icons.videocam_off,
          ),
          iconSize: 30,
        ),
        IconButton(
          onPressed: onAudioToggle,
          icon: Icon(
            audioEnabled == true ? Icons.mic : Icons.mic_off,
          ),
          iconSize: 30,
        ),
        const SizedBox(height: 20),
        Container(
          width: 70,
          color: Colors.red,
          child: IconButton(
            icon: Icon(Icons.call_end),
            color: Colors.white,
            onPressed: onMeetingEnd,
          ),
        ),
      ];
    } else {
      return [
        TextButton(
          onPressed: onReconnect,
          child: Text("YEniden bağlan"),
        ),
      ];
    }
  }
}
